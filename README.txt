*** NOTE ***

The code assumes you have a top-level directory called /output

Either change the code or add one. I've gitignored it so that outputs don't clutter the repo.

Use the requirements.txt file for needed packages.

Enjoy!

Author: Nick Burleigh for Wood Mackenzie.