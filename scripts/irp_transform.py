import pandas as pd

# We'll take the file directly from the L: drive
# If you don't have access to the L drive then overwrite this code with wherever the file is.
inpath = 'L:\\Research\\ENERGY STORAGE\\Storage models\\Utility-Scale Project Tracking\\IRP Coverage\\'
infile = 'IRP Tracker_LDrive.xlsm'

# Overwrite this with wherever you want the transformed file to end up on your local system.
outpath = 'C:\\Users\\burlni\\PycharmProjects\\IRP Data Transformation\\output\\'
outfile = 'IRP Tracker - Transformed.xlsx'

irp_import = pd.read_excel(inpath + infile, sheet_name='IRP Database', skiprows=1)

irp_import['id'] = irp_import.index

irp = pd.melt(irp_import, id_vars=['id', 'IRP Name', 'Utility', 'Parent Utility Company', 'State',
       'All States covered in IRP', 'Number of Customers State Territory',
       'Number of Customers per Utility', 'Region', 'Methodology', 'Link',
       'Release Date', 'Current or Outdated?', 'IRP Recurring Period (years)',
       'Anticipated Next Release', 'Anticipated Next Release Year',
       'Planning Year (initial)', 'Planning Year (final)',
       'Total Planning Period (years)', 'Target Renewable resouce Capacity',
       'Target Renewable Date', 'Technology Type',
       'Stand-alone or Paired (enter paired technology) ',
       'Solar Technology Notes', 'Storage Technology Notes',
       'Solar-plus-Storage Technology Notes', 'General IRP Notes',
       'LCOE $/MWh (high)', 'LCOE $/MWh (low)', 'Capital Cost $/kW (high)',
       'Capital Cost $/kW (low)', 'Fixed O&M ($/kW-yr)',
       'Variable O&M ($/MWh)', '$USD Reference Year',
       'Ownership model (PPA, utility-owned)', 'Contract term',
       'Capacity Replaced', 'Capacity Upgrades to existing Power Plants',
       'Combined Cycle Resources Additions', 'New Combined Turbine resources',
       'Storage Technology ', 'Average Duration ', 'Entry Type'],
              value_vars=['2019',
       '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028',
       '2029', '2030', '2031', '2032', '2033', '2034', '2035', '2036', '2037',
       '2038', '2039', '2040', '2041', '2042', '2043', '2044', '2045', '2046',
                          '2047', '2048', '2049', '2050'],
              var_name='Year',
              value_name='Capacity')

irp.to_excel(outpath + outfile, index=False)